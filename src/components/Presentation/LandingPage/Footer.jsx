import React from "react";
import { useTranslation } from "react-i18next";
import { Container } from "reactstrap";
import { ANDROIDWHITE, APPLEWHITE } from "../../../assets/images/Images";

export default function Footer() {
  const { t } = useTranslation();
  return (
    <footer>
      <Container>
        <h3 className="text-center">{t("footer.title")}</h3>
        <div className="text-center mt-4 footer-links">
          <a>
            <APPLEWHITE /> iOS APP
          </a>
          <a>
            {" "}
            <ANDROIDWHITE /> Android APP
          </a>
        </div>
      </Container>
    </footer>
  );
}
