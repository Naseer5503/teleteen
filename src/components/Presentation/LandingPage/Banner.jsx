import React from "react";
import { Col, Container, Row } from "reactstrap";
import { APPLE, images, WINDOW, ANDROID } from "../../../assets/images/Images";
import BANNER from "../../Container/LandingPage/Banner";
import { useTranslation } from "react-i18next";

export default function Banner() {
  const { t } = useTranslation();
  const { AVAILABLE_PLATFORMS } = BANNER();
  return (
    <Container>
      <Row md="2" xs="1" className="banner">
        <Col className="pt-md-5 pt-0 pb-md-0 pb-5">
          <img src={images.ArrowUp} alt="" />
          <h3>{t("banner.healthy_habits")}</h3>
          <p>{t("banner.for_your_kids")}</p>
          <div className="d-flex available-platform">
            {AVAILABLE_PLATFORMS.map((item, index) => {
              const { title, icon } = item;
              const renderIcon = (pram) => {
                switch (pram) {
                  case "iphone":
                    return <APPLE />;
                  case "window":
                    return <WINDOW />;
                  default:
                    return <ANDROID />;
                }
              };
              return (
                <div key={index}>
                  <div>{renderIcon(icon)}</div>
                  <p className="mt-1">{t(title)}</p>
                </div>
              );
            })}
          </div>
        </Col>
        <Col className="">
          <div className="img-card"></div>
        </Col>
      </Row>
    </Container>
  );
}
