import React, { useContext } from "react";
import LocaleContext from "../../../context/LocaleContext";
import TwoColumn from "./TwoColumn";

const Features = () => {
  const { json } = useContext(LocaleContext);
  return (
    <>
      {json?.features?.map((feature, index) => {
        const isEven = index % 2 === 0;
        const features = feature?.desc?.map((_, subIndex) => ({
          title: `features.${index}.desc.${subIndex}`,
        }));
        return (
          <TwoColumn
            key={index}
            title={`features.${index}.title`}
            isEven={isEven}
            features={features}
          />
        );
      })}
    </>
  );
};

export default Features;
