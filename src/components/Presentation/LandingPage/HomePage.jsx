import React from "react";
import Banner from "./Banner";
import Features from "./Features";
import HowItWorks from "./HowItWorks";
import WhatIsTeleteen from "./WhatIsTeleteen";
import Corporation from "./Corporation";
import Faq from "./Faq";
import Footer from "./Footer";

export default function HomePage() {
  return (
    <>
      <Banner />
      <HowItWorks />
      <WhatIsTeleteen />
      <Features />
      <Corporation />
      <Faq />
      <Footer />
    </>
  );
}
