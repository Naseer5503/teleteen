import React from "react";
import { useTranslation } from "react-i18next";
import { Col, Container, Row } from "reactstrap";

export default function TwoColumn({ title, features, isEven }) {
  const { t } = useTranslation();
  return (
    <Container>
      <Row
        md="2"
        xs="1"
        className={`two-column-left ${isEven && "two-column-right"}`}
      >
        <Col className="tw-column-text">
          <h3>{t(title)}</h3>
          <ul>
            {features.map((feature, index) => (
              <li key={index}>{t(feature?.title)}</li>
            ))}
          </ul>
        </Col>
        <Col>
          <div className="dummy-box"></div>
        </Col>
      </Row>
    </Container>
  );
}
