import React from "react";
import {
  Button,
  Collapse,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
  UncontrolledDropdown,
} from "reactstrap";
import { TELEGRAM } from "../../../assets/images/Images";
import header from "../../Container/LandingPage/Header";
import { useTranslation } from "react-i18next";

export default function Header() {
  const { t } = useTranslation();
  const { TOP_NAV, HandleChangeLanguage, isMenuOpen, handleToggleMenu } =
    header();

  return (
    <Container>
      <header className="mt-3">
        <Navbar color="transparent" expand="lg" light>
          <NavbarBrand href="/" color="red">
            TELETEENS
          </NavbarBrand>
          <NavbarToggler className="me-2" onClick={handleToggleMenu} />
          <Collapse navbar isOpen={isMenuOpen}>
            <Nav className="m-auto" navbar>
              {TOP_NAV.map((item, index) => {
                const { title, link, hasDropDown, dropdownList } = item;
                return (
                  <React.Fragment key={index}>
                    {hasDropDown ? (
                      <UncontrolledDropdown inNavbar nav>
                        <DropdownToggle caret nav>
                          {t(title)}
                        </DropdownToggle>
                        <DropdownMenu end>
                          {dropdownList.map((dropdownItem, index) => {
                            const { title } = dropdownItem;
                            return (
                              <DropdownItem key={index}>
                                {t(title)}
                              </DropdownItem>
                            );
                          })}
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    ) : (
                      <NavItem key={index}>
                        <NavLink href={link}>{t(title)}</NavLink>
                      </NavItem>
                    )}
                  </React.Fragment>
                );
              })}
            </Nav>
            <Nav className="navbar-right">
              <Button className="login d-flex align-items-center">
                {" "}
                <TELEGRAM />
                {t("download")}
              </Button>
              <select
                name="countries"
                className="form-select lang-select"
                onChange={(e) => {
                  HandleChangeLanguage(e);
                }}
              >
                <option value="en">🇺🇸</option>
                <option value="he">🇮🇱</option>
              </select>
            </Nav>
          </Collapse>
        </Navbar>
      </header>
    </Container>
  );
}
