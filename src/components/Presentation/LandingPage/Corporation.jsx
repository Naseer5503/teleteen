import React from "react";
import { useTranslation } from "react-i18next";
import { Container } from "reactstrap";

export default function Corporation() {
  const { t } = useTranslation();
  return (
    <div className="how-it-work corporation">
      <Container>
        <h3 className="text-center">{t("corporation")}</h3>
        <div className="d-grid">
          <div></div>
          <div></div>
          <div></div>
        </div>
      </Container>
    </div>
  );
}
