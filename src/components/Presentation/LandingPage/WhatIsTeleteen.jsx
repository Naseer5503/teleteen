import React, { useContext } from "react";
import { useTranslation } from "react-i18next";
import { Container } from "reactstrap";
import LocaleContext from "../../../context/LocaleContext";
export default function WhatIsTeleteen() {
  const { t } = useTranslation();
  const { json } = useContext(LocaleContext);
  return (
    <div className="how-it-work">
      <Container>
        <h3 className="text-center">{t("how_it_works.title")}</h3>
        <div className="d-grid">
          {json?.how_it_works?.steps?.map((_, index) => (
            <div key={index}>
              <p>
                {index}. {t(`how_it_works.steps.${index}.title`)}
              </p>
              <div className="box"></div>
            </div>
          ))}
        </div>
      </Container>
    </div>
  );
}
