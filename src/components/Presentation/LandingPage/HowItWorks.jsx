import React from 'react'
import { Col, Container, Row } from 'reactstrap'
import { useTranslation } from "react-i18next";

export default function HowItWorks() {
    const { t } = useTranslation();
    return (
        <Container>
            <Row
                md='2'
                xs='1'
                className='what-is-teleteen'>
                <Col className="pt-md-5 pt-0 pb-md-0 pb-5">
                    <div className='dummy-box'></div>
                </Col>
                <Col className="text-box">
                    <h3>{t('teleteens.title')}</h3>
                    <p>{t('teleteens.desc')}</p>
                </Col>

            </Row>
        </Container>

    )
}
