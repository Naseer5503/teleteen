import { useTranslation } from "react-i18next";
import {
  Accordion,
  AccordionBody,
  AccordionHeader,
  AccordionItem,
  Col,
  Container,
  Row,
} from "reactstrap";
import { images } from "../../../assets/images/Images";
import FAQ from "../../Container/LandingPage/Faq";

export default function Faq() {
  const { Support_list, handleToggle, open, Asked_Questions } = FAQ();
  const { t } = useTranslation();
  return (
    <Container>
      <Row className="faq" md='2'
        xs='1'>
        <Col className="support">
          <h2>{t("Support")}</h2>
          <div className="d-grid">
            {Support_list.map((list, index) => {
              const { title, icon } = list;
              const renderIcon = (pram) => {
                switch (pram) {
                  case "gmail":
                    return images.Gmail;
                  case "telegram":
                    return images.Telegram;
                  default:
                    return images.Speaker;
                }
              };
              return (
                <div className="d-flex" key={index}>
                  <div className="icon">
                    <img src={renderIcon(icon)} alt="" />
                  </div>
                  <div>
                    <p>{t(title)}</p>
                  </div>
                </div>
              );
            })}
          </div>
        </Col>
        <Col className="faq-section">
          <h2>{t("frequently_asked.title")}</h2>
          <div className="faq-accordion">
            <Accordion open={open} toggle={handleToggle}>
              {Asked_Questions.map(({ title, desc }, index) => {
                return (
                  <AccordionItem>
                    <AccordionHeader targetId={index}>
                      {t(title)}
                    </AccordionHeader>
                    <AccordionBody accordionId={index}>
                      {desc ? t(desc) : ""}
                    </AccordionBody>
                  </AccordionItem>
                );
              })}
            </Accordion>
          </div>
          <div className="d-flex bottom-link">
            <p>{t("more_questions")}</p>
            <div>
              <div className="right-arrow">
                <img src={images.ArrowRight} alt="" />
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
