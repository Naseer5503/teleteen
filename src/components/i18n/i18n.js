import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";
import Backend from "i18next-http-backend";

i18n
  .use(
    new Backend(null, {
      loadPath: "/locales/{{lng}}/{{lng}}.json",
    })
  )
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: "en",
    lng: "en",
    whitelist: ["en", "he", "es", "hi", "ar", "fa", "ru", "pt", "uk"],
    debug: true,

    // keySeparator: false, // we use content as keys

    interpolation: {
      escapeValue: false, // not needed for react!!
      formatSeparator: ",",
    },

    react: {
      wait: true,
      useSuspense: false,
    },
  });

export default i18n;
