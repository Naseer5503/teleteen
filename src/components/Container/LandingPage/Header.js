import { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import LocaleContext from "../../../context/LocaleContext";

const TOP_NAV = [
  {
    title: "download",
    link: "/",
    hasDropDown: true,
    dropdownList: [
      { title: "ios_app", link: "/" },
      { title: "android_app", link: "/" },
      { title: "windows", link: "/" },
    ],
  },
  { title: "premium", link: "/", hasDropDown: false },
  { title: "faq", link: "/", hasDropDown: false },
  { title: "Support", link: "/", hasDropDown: false },
];

function HEADER() {
  const { i18n } = useTranslation();
  const { setLocale } = useContext(LocaleContext);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const HandleChangeLanguage = (e) => {
    const lang = e.target.value;
    i18n.changeLanguage(lang);
    setLocale(lang);
  };
  const handleToggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };
  return { HandleChangeLanguage, TOP_NAV, isMenuOpen, handleToggleMenu };
}

export default HEADER;
