import { useState } from "react";

const Support_list = [
  { title: "support.0", icon: "gmail" },
  { title: "support.1", icon: "telegram" },
  { title: "support.2", icon: "group" },
];

const Asked_Questions = [
  {
    title: "frequently_asked.questions.0.title",
    desc: "frequently_asked.questions.0.desc",
  },
  { title: "frequently_asked.questions.1.title" },
  { title: "frequently_asked.questions.2.title" },
  { title: "frequently_asked.questions.3.title" },
  { title: "frequently_asked.questions.4.title" },
];

function FAQ() {
  const [open, setOpen] = useState("0");
  const handleToggle = (value) => setOpen(value);
  return { Support_list, open, handleToggle, Asked_Questions };
}
export default FAQ;
