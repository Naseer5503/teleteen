const AVAILABLE_PLATFORMS = [
    { title: 'iPhone/iPad', icon: 'iphone' },
    { title: 'Android', icon: 'android' },
    { title: 'Windows', icon: 'window' }
];

function BANNER() {
    return {AVAILABLE_PLATFORMS};
}
export default BANNER;