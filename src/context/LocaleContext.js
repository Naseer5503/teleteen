import React from "react";

const defaultValue = {
  locale: "en",
  setLocale: () => {},
  json: {},
};

export default React.createContext(defaultValue);
