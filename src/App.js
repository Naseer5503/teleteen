import { Suspense, useState } from "react";
import i18n from "./components/i18n/i18n";
import Header from "./components/Presentation/LandingPage/Header";
import HomePage from "./components/Presentation/LandingPage/HomePage";
import LocaleContext from "./context/LocaleContext";
import json from "./constants/app.json";

function App() {
  const [locale, setLocale] = useState(i18n.language);

  i18n.on("languageChanged", () => setLocale(i18n.language));

  return (
    <>
      <LocaleContext.Provider value={{ locale, setLocale, json }}>
        <Suspense fallback="Loading...">
          <Header />
          <HomePage />
        </Suspense>
      </LocaleContext.Provider>
    </>
  );
}

export default App;
